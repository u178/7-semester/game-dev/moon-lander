import h2d.col.Point;
import h2d.Scene;
import h2d.Graphics;


class Spacecraft {
    private var location: Point;
    private var velocity: Point;
    private var acceleration: Point;
    private var angle: Float;
    private var weight: Float = 1.;
    private var fuel: Float;
    private var engineOn: Bool;
    
    private var gameWidth: Int;

    private var engineTimer: Float;
    
    private var g: h2d.Graphics;
    private var polygonVertexes: Array<Point>;
    private var collisionVertexes: Array<Point>;
    private var exhaustVertexes: Array<Point>;

    private var moving: Bool = false;
    private var isDestroyed: Bool = false;

    private var state: String;

    public function new(scene: h2d.Scene, gameWidth) {
        this.g = new h2d.Graphics(scene);
        this.gameWidth = gameWidth;
        this.init();
        
        

        this.collisionVertexes = new Array();
        this.collisionVertexes.push(new Point(-10, -0));
        this.collisionVertexes.push(new Point(-10, -18));
        this.collisionVertexes.push(new Point(0, -24));
        this.collisionVertexes.push(new Point(10, -18));
        this.collisionVertexes.push(new Point(10, 0));
        this.collisionVertexes.push(new Point(-10, 0));

        this.polygonVertexes = new Array();

        this.polygonVertexes[0] = new Point(-5, 0);
        this.polygonVertexes.push(new Point(-2, -9));

        this.polygonVertexes.push(new Point(-8, -11));
        this.polygonVertexes.push(new Point(-10, -14));
        this.polygonVertexes.push(new Point(-8, -17));
        this.polygonVertexes.push(new Point(-4, -18));
        this.polygonVertexes.push(new Point(-2, -23));

        this.polygonVertexes.push(new Point(2, -23));
        this.polygonVertexes.push(new Point(4, -18));
        this.polygonVertexes.push(new Point(8, -17));
        this.polygonVertexes.push(new Point(10, -14));
        this.polygonVertexes.push(new Point(8, -11));
        this.polygonVertexes.push(new Point(2, -9));
        this.polygonVertexes.push(new Point(5, 0));



        this.exhaustVertexes = new Array();
        this.exhaustVertexes.push(new Point(-7, 3));
        this.exhaustVertexes.push(new Point(0, 9));
        this.exhaustVertexes.push(new Point(7, 3));
    }

    public function init(reinit: Bool = true) {
        if (reinit) {
           this.fuel = Config.startingFuel;
        }
        this.location = new h2d.col.Point(200., 400.);
        this.acceleration = new Point(0, 0);
        this.velocity = new Point(0, 0);

        this.angle = 90.;
        this.state = "straight";
        
        this.engineTimer = 0;
        this.engineOn = false;
        this.moving = true;
        this.isDestroyed = false;
    }


    public function getFuel() {
        return this.fuel;
    }

    public function getHorizontalSpeed() {
        return this.velocity.x;
    }

    public function getVerticalSpeed() {
        return this.velocity.y;
    }

    public function getVertices(): Array<Point> {
        return this.collisionVertexes.copy();
    }

    public function getLocation(): Point {
        return this.location;
    }
    
    public function getAngle(): Float {
        return this.angle;
    }
    
    public function setLocation(location: Point) {
        this.location = location;
    }

    public function setVelocity(velocity: Point) {
        this.velocity = velocity;
    }

    public function land() {
        this.moving = false;
        this.velocity.x = 0;
        this.velocity.y = 0;
    }

    public function loseFuel(amount: Float) {
        this.fuel -= Math.min(amount, this.fuel);
    }

    public function draw() {
        this.g.clear();
        if (this.isDestroyed) {
            return true;
        }
        this.g.lineStyle(1, 0xFFFFFF);
        var radianAngle = (90-this.angle) * Math.PI / 180;
        var cosA = Math.cos(radianAngle);
        var sinA = Math.sin(radianAngle);
        for (i in 0...this.polygonVertexes.length) {
            var newX = Math.cos(radianAngle) * this.polygonVertexes[i].x - Math.sin(radianAngle) * this.polygonVertexes[i].y + this.location.x;
            var newY = Math.sin(radianAngle) * this.polygonVertexes[i].x + Math.cos(radianAngle) * this.polygonVertexes[i].y + this.location.y;
            this.g.addVertex(newX, newY, 0xFF, 0xFF, 0xFF, 1.);
        }

        if (Config.drawColissionBox) {
        this.g.lineStyle(1, 0x00FFFF);

            for (i in 0...this.collisionVertexes.length) {
                var newX = cosA * this.collisionVertexes[i].x - sinA * this.collisionVertexes[i].y + this.location.x;
                var newY = sinA * this.collisionVertexes[i].x + cosA * this.collisionVertexes[i].y + this.location.y;
                this.g.addVertex(newX, newY, 0x01, 0xFF, 0x01, 1.);
            }   
        }
        
        
        if (this.engineOn) {
            this.g.lineStyle(2, 0xFF0000);
            for (i in 0...this.exhaustVertexes.length) { 
                var newX = Math.cos(radianAngle) * this.exhaustVertexes[i].x - Math.sin(radianAngle) * this.exhaustVertexes[i].y + this.location.x;
                var newY = Math.sin(radianAngle) * this.exhaustVertexes[i].x + Math.cos(radianAngle) * this.exhaustVertexes[i].y + this.location.y;
                this.g.addVertex(newX, newY, 0xFF, 0xFF, 0xFF, 1.);
            }
        }
        return true;       
        
        
    }

    public function update(dt: Float) {
        if (!this.moving) {
            return false;
        }
        if (this.state == "right") {
            this.angle -= dt * Config.angularVelocity;
        } else if (this.state == "left") {
            this.angle += dt * Config.angularVelocity;
        }

        this.location.x += this.velocity.x * dt  + this.acceleration.x * dt * dt / 2; //v0t + at^2/2;
        this.location.y += this.velocity.y * dt  + this.acceleration.y * dt * dt / 2; //v0t + at^2/2;

        
        if (this.location.x > this.gameWidth) {
            this.location.x = this.location.x - this.gameWidth;
        }

        if (this.location.x < 0) {
            this.location.x = this.gameWidth - this.location.x;
        }

        // set acceleration
        if (this.engineOn) {
            this.acceleration.y = Config.gravitation - Config.engineThrust * Math.sin(this.angle * Math.PI / 180);
            this.acceleration.x = Config.windSpeed   + Config.engineThrust * Math.cos(this.angle * Math.PI / 180);
            // trace(this.acceleration.x);
            this.fuel -= dt * Config.starshipFuelConsumption;
            if (this.fuel < 0) {
                this.fuel = 0;
            }
        } else {
            this.acceleration.y = Config.gravitation;
            this.acceleration.x = Config.windSpeed;
        }

        // apply speed 
        this.velocity.x += this.acceleration.x * dt;
        this.velocity.y += this.acceleration.y * dt;
        
        return true;
    }

    public function turnEngineOff() {
        this.engineOn = false;
    }

    public function useEngine(): Void {
        if (this.fuel > 0) {
            this.engineOn = true;
        } else {
            this.engineOn = false;
        }
    }

    public function turnRight() {
        this.state = "right";
    }

    public function turnLeft() {
        this.state = "left";
    }

    public function goStraigth() {
        this.state = "straight";
    }

    public function destroy() {
        this.isDestroyed = true;
        
    }
}
class Config {


    //World cofings
    public static var windSpeed: Float = 0;
    public static var gravitation: Float = 9.8;

    // starship configs
    public static var startingFuel: Float = 1000;
    public static var starshipFuelConsumption: Float = 40;
    public static var angularVelocity: Float = 15;
    public static var engineThrust: Float = 20;
    public static var engineUsageTime: Float = 0.15;
    public static var shipWidth: Float = 10;
    
    // landing settings
    public static var landingMaxAngle: Float = 5;
    public static var hardLandingVelocity: Float = 10;
    public static var maxLandingVelocity: Float = 15;
    public static var crashFuelFine: Float = 200;
    public static var landingScore: Int = 100;
    public static var harshLandingScore: Int = 100;



    // Terrain configs
    public static var bottomPadding: Int = 20;
    public static var scope: Int = 230;

    public static var flatAreaChance: Float = 0.3;
    public static var maxAngle: Int = 7;
    public static var minArea: Int = 20;
    public static var maxArea: Int = 80;

    public static var seed: Int = 42;

    //Testing
    public static var drawColissionBox: Bool = false;
    public static var collisionLineFromMouseToCenter: Bool = false;
    
}
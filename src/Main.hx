import h2d.col.RoundRect;
import h2d.Interactive;
import hxd.res.Sound;
import h2d.HtmlText;
import hxd.App;
import h2d.Scene;
import h2d.Tile;
import hxd.Window;
import h2d.Console;
import Config.Config;

class Main extends hxd.App {

	private var game: Game;
	override function init() {
		this.game = new Game(s2d);
	}
	


	override function update(dt:Float) {
		this.game.update(dt);
	}

	static function main() {
		new Main();
	}

}
import hxd.Rand;
import h3d.prim.Primitive;
import h2d.col.Point;
import h2d.Scene;
import h2d.Graphics;
import hxd.Perlin;

class Terrain {
    private var vertexes: Array<Point>;
    private var g: h2d.Graphics;
    private var gameWidth: Int;

    public function new(scene: h2d.Scene, _gameWidth) {
        this.g = new h2d.Graphics(scene);
        this.vertexes = new Array();
        this.gameWidth = _gameWidth;
        // this.generateFlatTerrain();
        
    }
    
    public function generatePerlinTerrain() {
        var noise = new Perlin();
        var x: Float = 0;
        var height = this.g.getScene().height;
        var y_min : Float= 1000000.;
        var y_max: Float = -1.;
        while (x <= this.gameWidth) {
            var y: Float = noise.perlin1D(Config.seed, x / this.gameWidth , 10, 4, 1.5);
            
            if (y > y_max) {
                y_max = y;
            }
            
            if (y < y_min) {
                y_min = y;
            }

            this.vertexes.push(new Point(x, y));
            
            x += 1;

            // var rnd = new Rand(123);
            // x += rnd.random(Config.maxArea - Config.minArea) + Config.minArea;
        }
        //trace("game width: " + gameWidth);
        // trace("x:" + x);


        for (i in 0...this.vertexes.length) {
            this.vertexes[i].y = height - ((this.vertexes[i].y - y_min) / (y_max + Math.abs(y_min)) * (Config.scope) + Config.bottomPadding);
        }
    }

    public function generatePerlinTerrain2() {
        var noise = new Perlin();
        var x: Float = 0;
        var height = this.g.getScene().height;
        var y_min : Float= 1000000.;
        var y_max: Float = -1.;
        this.vertexes = new Array<Point>();
        while (x <= this.gameWidth) {
            var y: Float = noise.perlin1D(Config.seed, x / this.gameWidth , 10, 4, 1.5);
            
            if (y > y_max) {
                y_max = y;
            }
            
            if (y < y_min) {
                y_min = y;
            }

            this.vertexes.push(new Point(x, y));
            

            var rnd = Rand.create();
            x += rnd.random(Config.maxArea - Config.minArea) + Config.minArea;
        }
        //trace("game width: " + gameWidth);
        // trace("x:" + x);


        for (i in 0...this.vertexes.length) {
            this.vertexes[i].y = height - ((this.vertexes[i].y - y_min) / (y_max + Math.abs(y_min)) * (Config.scope) + Config.bottomPadding);
        }

        var rnd = Rand.create();
        var flatSufacesNumber = rnd.random(5) + 1;

        for (i in 0...flatSufacesNumber) {
            var flatSegmentIndex = rnd.random(this.vertexes.length-1);
            this.vertexes[flatSegmentIndex+1].y = this.vertexes[flatSegmentIndex].y;
        }
        this.vertexes[this.vertexes.length-1].y = this.vertexes[0].y;
    
    }

    public function generateFlatTerrain() {
        this.vertexes = new Array<Point>();

        var x = 0;
        while (x <= this.gameWidth) {


            this.vertexes.push(new Point(x, 600));
            
            var rnd = new Rand(123);
            x += rnd.random(Config.maxArea - Config.minArea) + Config.minArea;
        }
    }


    public function draw() {
        this.g.clear();
        this.g.lineStyle(4, 0xFFFFFF);
        for (i in 0...this.vertexes.length) {
            this.g.addVertex(this.vertexes[i].x, this.vertexes[i].y, 0xFF, 0xFF, 0xFF, 1);
            //trace(this.vertexes[i].x + ' ' + this.vertexes[i].y);
       
        }
        this.g.lineStyle(2, 0xffffff);

        for (i in 0...this.vertexes.length) {
            this.g.drawCircle(this.vertexes[i].x, this.vertexes[i].y, 2);
            //trace(this.vertexes[i].x + ' ' + this.vertexes[i].y);
       
        }
        // trace(this.vertexes.length);
    }

    public function getVertices(): Array<Point> {
        return this.vertexes.copy();
    }

}
package utils;

class Miscellaneous {
    public static function twoLinesIntersect(p1: h2d.col.Point, p2: h2d.col.Point, 
        p3: h2d.col.Point, p4: h2d.col.Point) {
        
        var uA: Float = ((p4.x-p3.x)*(p1.y-p3.y) - (p4.y-p3.y)*(p1.x-p3.x)) / ((p4.y-p3.y)*(p2.x-p1.x) - (p4.x-p3.x)*(p2.y-p1.y));
        var uB: Float = ((p2.x-p1.x)*(p1.y-p3.y) - (p2.y-p1.y)*(p1.x-p3.x)) / ((p4.y-p3.y)*(p2.x-p1.x) - (p4.x-p3.x)*(p2.y-p1.y));

        if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
            return true;
        }
        return false;
    } 

    public static function PolygonLineIntersects(vertices: Array<h2d.col.Point>, p1: h2d.col.Point, p2: h2d.col.Point) {
        //ToDo use angle from Spacecraft.hx
        var next: Int = 1;
        for (i in 0...vertices.length) {
            next = i + 1;
            if (next == vertices.length) {
                next = 0;
            }

            if (Miscellaneous.twoLinesIntersect(vertices[i], vertices[next], p1, p2)) {
                return true;
            }
        }

        return false;
    }


}
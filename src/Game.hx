import h3d.prim.Primitive;
import utils.Miscellaneous;
import h2d.col.Point;
import hxd.fmt.hmd.Writer;
import format.abc.Data.ABCData;

class Game {

    var score: Int = 0;
    var bestScore: Int = 0;
    var gameWidth: Int = 0;
    
    private var sp: Spacecraft;
	private var terrain: Terrain;

    private var g: h2d.Graphics; 
    private var scene: h2d.Scene;
    private var terrainVerices: Array<Point>;


    // labels
    private var scoreLabelText: h2d.Text;
    private var scoreLabelValue: h2d.Text;

    private var fuelLabelText: h2d.Text;
    private var fuelLabelValue: h2d.Text;

    private var horSpeedLabelText: h2d.Text;
    private var horSpeedLabelValue: h2d.Text;

    private var verSpeedLabelText: h2d.Text;
    private var verSpeedLabelValue: h2d.Text;

    private var playerTextLabel: h2d.Text;

    private var gamePaused: Bool = false;

    private var text: String = "";


    public function new(scene: h2d.Scene) {
        this.scene = scene;
        this.gameWidth = this.scene.width;

        this.init();
        this.g = new h2d.Graphics(scene);

    }

    function init() {

		this.sp = new Spacecraft(this.scene, gameWidth);
		this.scene.addEventListener(keyEvent);

		this.terrain = new Terrain(this.scene, gameWidth);
        // this.terrain.generateFlatTerrain();
		this.terrain.generatePerlinTerrain2();
		
        this.terrain.draw();
        this.terrainVerices = this.terrain.getVertices();
        this.initLabels();
        this.text = "";
        this.gamePaused = false;
        this.setPlayerText("");
    }

    public function update(dt:Float) {
        this.draw();		
		this.sp.update(dt);  
        this.checkColision();


        this.scoreLabelValue.text = Std.string(this.score);
        this.fuelLabelValue.text = Std.string(Std.int(this.sp.getFuel()));
        this.horSpeedLabelValue.text = Std.string(Std.int(this.sp.getHorizontalSpeed()));
        this.verSpeedLabelValue.text = Std.string(Std.int(this.sp.getVerticalSpeed()));
    }

    public function draw() {
        this.g.clear();
        //this.terrain.draw();
        this.sp.draw();
        if (Config.collisionLineFromMouseToCenter) {
            this.mouseToCenterCollisionCheck();
        }		
    }

    private function mouseToCenterCollisionCheck() {
        var startPoint: h2d.col.Point = new Point(this.scene.mouseX, this.scene.mouseY);
        var centerPoint: h2d.col.Point = new Point(this.scene.width / 2, this.scene.height / 2);
        
        var points: Array<h2d.col.Point> = this.sp.getVertices();
        var spLoc: Point = this.sp.getLocation();
        
        var radianAngle = (90-this.sp.getAngle()) * Math.PI / 180;
        var cosA = Math.cos(radianAngle);
        var sinA = Math.sin(radianAngle);
        for (i in 0...points.length) {
            points[i] = points[i].clone();

            var x = points[i].x;
            var y = points[i].y;

            points[i].x = cosA * x - sinA * y + spLoc.x;
            points[i].y = sinA * x + cosA * y + spLoc.y;
        }
        this.g.lineStyle(2, 0x00FF00);


        if (Miscellaneous.PolygonLineIntersects(points, startPoint, centerPoint)) {
            this.g.lineStyle(2, 0x00FF00);
        } else {
            this.g.lineStyle(2, 0xFFFFFF);
        }
        this.g.moveTo(startPoint.x, startPoint.y);
        this.g.lineTo(centerPoint.x, centerPoint.y);
    }

    private function checkColision() {
        if (this.sp.getLocation().y > this.scene.height) {
            this.crush();
        }

        if (this.gamePaused) {
            return true;
        }
        // Generate ship points
        var points: Array<h2d.col.Point> = this.sp.getVertices();
        var spLoc: Point = this.sp.getLocation();
        
        var radianAngle = (90-this.sp.getAngle()) * Math.PI / 180;
        var cosA = Math.cos(radianAngle);
        var sinA = Math.sin(radianAngle);
        
        var xMax: Float = -10000;
        var xMin: Float = 10000;
        for (i in 0...points.length) {
            points[i] = points[i].clone();

            var x = points[i].x;
            var y = points[i].y;

            points[i].x = cosA * x - sinA * y + spLoc.x;
            points[i].y = sinA * x + cosA * y + spLoc.y;

            if (points[i].x > xMax) {xMax = points[i].x;}

            if (points[i].x < xMin) {xMin = points[i].x;}
        }
        // check ship surface collision
        for (i in 0...terrainVerices.length-1) {
            var startPoint = terrainVerices[i];
            var finishPoint = terrainVerices[i+1];
            var gearTouched = false;
            var touchedSurface = false;

            // if (xMin > startPoint.x && xMax<finishPoint.x) {
                if (Miscellaneous.PolygonLineIntersects(points, startPoint, finishPoint)) {
                   
                    touchedSurface = true;
                    

                    // if (i != points.length-1) {
                    //     trace("crush");
                    //     this.crush();
                    //     this.lose();
                    // } else {
                    //     trace("land");
                    //     land(points, startPoint, finishPoint);
                    // }
                }
                if (touchedSurface) {
                    land(points, startPoint, finishPoint);

                    // if (gearTouched) {
                        // trace("gear touched!");
                        // land(points, startPoint, finishPoint);
                    // } else {
                        //trace("no gear touch");
                        //this.crush();
                    // }
                }


            // }
        }
        return true;
    }


    private function land(spPoints: Array<Point>, p1: Point, p2: Point) {
        if (p1.y != p2.y) {
            //trace("lines not parallel");
            this.crush();    
            return false;
        }
        if (p1.y < this.sp.getLocation().y - Config.shipWidth || p2.y > this.sp.getLocation().y + Config.shipWidth) {
            //trace("out of landing zone");
            //horSpeedLabelText.text = "out of LZ";
            this.crush();
            return false;
        }
        if (Math.abs(this.sp.getAngle() - 90)  > Config.maxAngle) {
            //trace("rotated too much!");
            //horSpeedLabelText.text = "rotated";

            this.crush();
            return false;
        }

        if (this.sp.getVerticalSpeed()  > Config.maxLandingVelocity) {
            //trace("too fast!");
            //horSpeedLabelText.text = "too fast";
            
            this.crush();
            return false;
        }
        
        if (this.sp.getVerticalSpeed()  > Config.hardLandingVelocity) {
            this.successfulLand(true);
            return true;
        }
        this.successfulLand();
        return true;
        
    }

    private function lose() {
        
    }

    private function successfulLand(harsh: Bool = false) {
        //trace("sp Landed");
        this.gamePaused = true;

        if (harsh) {
            this.text = 'harsh Landing!\n Be Careful';
            this.setPlayerText('harsh Landing!\n Be Careful\n Press Enter to play again');
            this.score += Config.harshLandingScore;
        } else {
            this.text = "successful landing!";
            this.setPlayerText("successful landing!\n Press Enter to play again");
            this.score += Config.landingScore;
        }
        this.sp.land();
    }

    private function crush() {
        this.sp.destroy();
        this.sp.land();
        this.gamePaused = true;
        this.text = 'Spacecraft crashed. \n You\'ve lost ${Config.crashFuelFine}\n Press Enter to play again';
        this.setPlayerText('Spacecraft crashed. \n You\'ve lost ${Config.crashFuelFine}\n Press Enter to play again');

        this.sp.loseFuel(Config.crashFuelFine);

        // this.g.lineStyle(2, 0xff0000);
        // this.g.drawCircle(50, 50, 3);


        //trace("crush");
    }

    private function keyEvent(event: hxd.Event) {
		if (event.keyCode == 38 || event.keyCode == 87) { 
			// up or W

			switch(event.kind) {
				case EKeyDown: this.sp.useEngine();
				case EKeyUp: this.sp.turnEngineOff();
				case _:
			}
			
		} else if ((event.keyCode == 39 || event.keyCode == 68) && (event.keyCode == 37 || event.keyCode == 65 )) {
			// both right and left
			this.sp.goStraigth();
		} else if (event.keyCode == 39 || event.keyCode == 68) {
			// right or D
			switch(event.kind) {
				case EKeyDown: this.sp.turnRight();
				case EKeyUp: this.sp.goStraigth();
				case _:
			}
		} else if (event.keyCode == 37 || event.keyCode == 65) {
			// left or A
			switch(event.kind) {
				case EKeyDown: this.sp.turnLeft();
				case EKeyUp: this.sp.goStraigth();
				case _:
			}
		}

        if (event.keyCode == 13 && this.gamePaused) {
            if (this.sp.getFuel() > 0) {
               this.playOneMoreRound();
            } else {
                this.playOneMoreGame();
            }
        }
		}

    private function initLabels() {
        var textScale = 1.4;
        var paddingX = 30;
        var paddingY = 30;
        //score
        scoreLabelText = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        scoreLabelText.setScale(textScale);
        scoreLabelText.text = Std.string("SCORE");
		scoreLabelText.x = paddingX;
        scoreLabelText.y = paddingY;

        scoreLabelValue = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        scoreLabelValue.setScale(textScale);
        scoreLabelValue.text = Std.string("0");
		scoreLabelValue.x = paddingX + scoreLabelText.textWidth* textScale + 8;
        scoreLabelValue.y = paddingY;

        //fuel
        fuelLabelText = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        fuelLabelText.setScale(textScale);
        fuelLabelText.text = Std.string("FUEL");
		fuelLabelText.x = paddingX;
        fuelLabelText.y = paddingY + scoreLabelText.textHeight + 3;

        fuelLabelValue = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        fuelLabelValue.setScale(textScale);
        fuelLabelValue.text = Std.string(this.sp.getFuel());
		fuelLabelValue.x = paddingX + scoreLabelText.textWidth* textScale + 8;
        fuelLabelValue.y = paddingY + scoreLabelText.textHeight + 3;

        // horizontal speed
        horSpeedLabelText = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        horSpeedLabelText.setScale(textScale);
        horSpeedLabelText.text = Std.string("HORIZONTAL SPEED");
		horSpeedLabelText.x = this.scene.width - (paddingX + horSpeedLabelText.textWidth * textScale + scoreLabelText.calcTextWidth("000") * textScale + 8);
        horSpeedLabelText.y = paddingY;

        horSpeedLabelValue = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        horSpeedLabelValue.setScale(textScale);
        horSpeedLabelValue.text = Std.string(this.sp.getHorizontalSpeed());
		horSpeedLabelValue.x = this.scene.width - (paddingX + scoreLabelText.calcTextWidth("000") * textScale);
        horSpeedLabelValue.y = paddingY;

        // vertical speed
        verSpeedLabelText = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        verSpeedLabelText.setScale(textScale);
        verSpeedLabelText.text = Std.string("VERTICAL SPEED");
		verSpeedLabelText.x = this.scene.width - (paddingX + horSpeedLabelText.textWidth * textScale + scoreLabelText.calcTextWidth("000") * textScale + 8);
        verSpeedLabelText.y = paddingY + horSpeedLabelValue.textHeight + 3;

        verSpeedLabelValue = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        verSpeedLabelValue.setScale(textScale);
        verSpeedLabelValue.text = Std.string(this.sp.getVerticalSpeed());
		verSpeedLabelValue.x = this.scene.width - (paddingX + scoreLabelText.calcTextWidth("000") * textScale);
        verSpeedLabelValue.y = paddingY + horSpeedLabelValue.textHeight + 3;
    
        
        playerTextLabel = new h2d.Text(hxd.res.DefaultFont.get(), this.scene);
        playerTextLabel.setScale(textScale);
        playerTextLabel.text = this.text;
    }  

    private function setPlayerText(t: String) {
        this.playerTextLabel.text = t;
        this.playerTextLabel.x = (this.scene.width - this.playerTextLabel.textWidth * 2) / 2;
        this.playerTextLabel.y = (this.scene.height - this.playerTextLabel.textHeight * 2) / 2;
    }

    private function playOneMoreRound() {
        this.sp.init(false);
        this.gamePaused = false;
        this.setPlayerText("");
    }

    private function playOneMoreGame() {
        this.sp.init(true);
        this.gamePaused = false;
        this.setPlayerText("");
        this.terrain.generatePerlinTerrain2();
        this.terrain.draw();
    }



}